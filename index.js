const express = require('express')
const fs = require('fs')
const bodyParser = require('body-parser')
const uploadFolder = './uploads/'
const app = express()
const morgan = require('morgan')

app.use(bodyParser.json())
app.use(morgan('combined'))

const PORT = process.env.PORT || 8080

app.get('/', (req, res) => {
    res.status(400).json({
        message: 'Client error'
    })
})

app.get('/api/files',async (req, res) => {
    try {
        let output = {
            message: 'Success',
        }

        await fs.promises.readdir(uploadFolder).then(res => {
            output.files = res
        })

        return res.status(200).json(output)
    } catch (err) {
        console.log('Error', err)

        res.status(500).json({
            message: 'Server error'
        })
    }
})

app.get('/api/files/:filename', async (req, res) => {
    try {
        const filename = req.params.filename
        let fileContent = ''

        if (!fs.existsSync(uploadFolder + filename)) {
            return res.status(400).json({
                message: `No file with '${filename}' filename found`
            })
        }

        await fs.promises.readFile(uploadFolder + filename, {
            encoding: "utf8",
        }).then((data) => {
            fileContent = data
        }).catch(err => {
            console.log('Cannot read file', err)

            res.status(400).json({
                message: 'Cannot read file',
                err
            })
        })

        return res.status(200).json({
            message: 'Success',
            filename,
            content: fileContent,
            extension: filename.match(/\.[0-9a-z]+$/i)[0].replace('.', ''),
            uploadedDate: fs.statSync(uploadFolder + filename).birthtime,
        })
    } catch (err) {
        console.log('Error', err)

        res.status(500).json({
            message: 'Server error'
        })
    }
})

app.post('/api/files',(req, res) => {
    try {
        const body = req.body

        if (!body.filename) return res.status(400).json({
            message: "Please specify 'filename' parameter"
        })

        if (!body.content) return res.status(400).json({
            message: "Please specify 'content' parameter"
        })

        if (fs.existsSync(uploadFolder + body.filename)) return res.status(400).json({
            message: 'File already exists'
        })

        fs.writeFile(uploadFolder + body.filename, body.content,(err) => {
            if (err) res.status(400).json({
                message: 'Wooops... Unresolved error',
                err
            })

            return res.status(200).json({
                message: 'File created successfully'
            })
        })
    } catch (err) {
        console.log('Error', err)

        res.status(500).json({
            message: 'Server error'
        })
    }
})


app.listen(PORT, () => console.log(`Example app listening at http://localhost:${PORT}`))